import Realm from 'realm';
import {FilterSchema, SubtaskSchema, TaskSchema} from '../schemas/TaskSchema';
import {RoutineSchema} from '../schemas/RoutineSchema';
import {
  CourseSchema,
  FlashCardSchema,
  NotificationStudySchema,
  RepetitionTimeSchema,
  PomodoroSchema,
  ExamsSchema,
  NotificationsExamsSchema,
} from '../schemas/CourseSchema';

export function getRealmApp() {
  const appId = 'skoolrealmdb-lvuzo';
  const appConfig = {
    id: appId,
    timeout: 10000,
    app: {
      name: 'default',
      version: '0',
    },
  };
  return new Realm.App(appConfig);
}

export function getRealm() {
  const realmApp = getRealmApp();
  const realm = Realm.open(
    realmApp.currentUser
      ? {
          schema: [FilterSchema, SubtaskSchema, TaskSchema, RoutineSchema],
          schemaVersion: 1,
          sync: {
            user: realmApp.currentUser,
            partitionValue: realmApp.currentUser.id,
          },
        }
      : {
          schema: [FilterSchema, SubtaskSchema, TaskSchema, RoutineSchema],
          schemaVersion: 1,
        },
  );
  return realm;
}

export const isLoggedIn = realmApp => {
  return realmApp && realmApp.currentUser && realmApp.currentUser.isLoggedIn;
};
