export const TaskSchema = {
  name: 'Task',
  properties: {
    _id: 'objectId?',
    id: 'string?',
    alarmNotifIds: 'int[]',
    color: 'string?',
    done: 'bool?',
    filter: 'Filter',
    icon: 'string?',
    mode: 'int?',
    name: 'string?',
    pomodoro: 'string?',
    soundDay: 'int?',
    soundHour: 'int?',
    soundMinute: 'int?',
    soundMonth: 'int?',
    soundYear: 'int?',
    subtasks: 'Subtask[]',
    userID: 'string?',
  },
  primaryKey: '_id',
};

export const FilterSchema = {
  name: 'Filter',
  embedded: true,
  properties: {
    id: 'string?',
    name: 'string?',
  },
};

export const SubtaskSchema = {
  name: 'Subtask',
  embedded: true,
  properties: {
    done: 'string?',
    id: 'string?',
    name: 'string?',
  },
};