const RoutineSchema = {
  name: 'Routine',
  properties: {
    _id: 'objectId',
    userID: 'string',
    name: 'string',
    description: 'string',
    colorPosition: 'int',
    public: 'bool',
    tasks: 'Task[]',
  },
  primaryKey: '_id',
};

export {RoutineSchema};
