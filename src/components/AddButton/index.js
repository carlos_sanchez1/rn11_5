import React, {useState, useEffect, useRef, useContext} from 'react';
import {View, Text, TextInput, ActivityIndicator, Alert} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import BottomModal from '../BottomModal';
import Button from '../Button';
import Input from '../Input';

import SettingsOptionsContext from '../../contexts/SettingsOptionsContext';
import RealmContext from '../../contexts/RealmContext';
import {getRealm, getRealmApp, isLoggedIn} from '../../services/realm';

import {useTheme} from '@react-navigation/native';
import {
  getSettingsData,
  storeSettingsData,
  showAyncStorageData,
  removeSettingsData,
  storeSettingsEncryptedData,
  getSettingsEncryptedData,
  isEmpty,
} from '../../utils';

const AddButton = ({iconSize, onPress, customButton}) => {
  const {colors} = useTheme();
  const {realmApp, setRealmApp, realm, setRealm} = useContext(RealmContext);

  const loginrefBottomModal = useRef();
  const signinrefBottomModal = useRef();
  const forgotPasswordefBottomModal = useRef();

  const [name, setName] = useState('');
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [viewPassword, setViewPassword] = useState(false);

  const {auth, setAuth} = useContext(SettingsOptionsContext);

  const [authError, setAuthError] = useState({
    error: false,
    errorMessage: '',
  });

  // const [auth, setAuth] = useState(null);
  const [authLoading, setAuthLoading] = useState(false);

  // const handleRegister = async (email_, password_) => {
  //   try {
  //     // setAuthLoading(true);
  //     const user = await realmApp.emailPasswordAuth.registerUser(
  //       email_,
  //       password_,
  //     );
  //   } catch (err) {
  //     // setAuthLoading(false);
  //     setAuthError(prevState => ({
  //       ...prevState,
  //       error: true,
  //       errorMessage: err.message,
  //     }));
  //     console.error('Failed to register', err);
  //   }
  // };

  const handleRegister = async (email_, password_, username_, name_) => {
    if (isEmpty(name_)) {
      Alert.alert('Name is required');
      return;
    }

    if (isEmpty(username_)) {
      Alert.alert('Username is required');
      return;
    }

    if (isEmpty(email_)) {
      Alert.alert('Email is required');
      return;
    }

    if (isEmpty(password_)) {
      Alert.alert('Password is required');
      return;
    }

    setAuthLoading(true);

    try {
      await realmApp.emailPasswordAuth.registerUser(email_, password_);
      const emailPasswordUserCredentials = Realm.Credentials.emailPassword(
        email_,
        password_,
      );
      const anonymousUser = await realmApp.logIn(Realm.Credentials.anonymous());
      const res = await anonymousUser.callFunction('addUserData', {
        name: name_,
        username: username_,
        email: email_,
      });

      console.log(anonymousUser);
      console.log(res);
      if (
        res.status === 'success' &&
        anonymousUser &&
        anonymousUser.isLoggedIn
      ) {
        await anonymousUser.linkCredentials(emailPasswordUserCredentials);
        setRealmApp(getRealmApp());
        setRealm(await getRealm());
        signinrefBottomModal.current.close();
      } else {
        Alert.alert('Error: ' + res.message);
      }
      setAuthLoading(false);
    } catch (err) {
      // if (err.message.includes('already in use')) {
      //   Alert.alert('Email address already in use');
      //   return;
      // }
      setAuthLoading(false);
      setAuthError(prevState => ({
        ...prevState,
        error: true,
        errorMessage: err.message,
      }));
      console.error('Failed to log in', err);
    }
  };

  const handleLogin = async (emaill, passwordd) => {
    setAuthLoading(true);
    try {
      const credentials = Realm.Credentials.emailPassword(
        emaill.toLowerCase(),
        passwordd,
      );
      const user = await realmApp.logIn(credentials);
      if (user && user.isLoggedIn) {
        setRealmApp(getRealmApp());
        setRealm(await getRealm());
        loginrefBottomModal.current.close();
      }
      setAuthLoading(false);
    } catch (err) {
      setAuthLoading(false);
      setAuthError(prevState => ({
        ...prevState,
        error: true,
        errorMessage: err.message,
      }));
      console.error('Failed to log in', err);
    }
  };

  const handleSendEmailToResetPassword = async email_ => {
    try {
      await realmApp.emailPasswordAuth.sendResetPasswordEmail(email_);
    } catch (err) {
      console.error('Failed to send email to reset password', err);
    }
    forgotPasswordefBottomModal.current.close();
  };

  const loginModal = () => {
    return (
      <BottomModal
        openModal={loginrefBottomModal}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={840}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        content={
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                paddingRight: 15,
                marginTop: 50,
              }}>
              <AntDesign
                onPress={() => {
                  setAuthError(prevState => ({
                    ...prevState,
                    error: false,
                    errorMessage: '',
                  }));
                  loginrefBottomModal.current.close();
                }}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                height: '87%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <View
                  style={{
                    marginBottom: 50,
                    // backgroundColor: 'pink',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Ionicons name="school" color={colors.text} size={45} />
                  <Text
                    style={{
                      fontSize: 38,
                      marginLeft: 4,
                      fontWeight: 'bold',
                      color: colors.text,
                    }}>
                    Skool
                  </Text>
                </View>

                <View>
                  <TextInput
                    value={email}
                    onChangeText={value => setEmail(value)}
                    placeholder="Email"
                    placeholderTextColor="#ADADAF"
                    style={{
                      backgroundColor: colors.forms,
                      color: colors.text,
                      paddingVertical: 15,
                      paddingHorizontal: 25,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />
                  <TextInput
                    value={password}
                    onChangeText={value => setPassword(value)}
                    placeholder="Password"
                    placeholderTextColor="#ADADAF"
                    secureTextEntry={viewPassword ? false : true}
                    style={{
                      backgroundColor: colors.forms,
                      color: colors.text,
                      paddingVertical: 15,
                      paddingHorizontal: 25,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />
                  {viewPassword ? (
                    <Ionicons
                      name="eye-off-outline"
                      color="#ADADAF"
                      size={15}
                      style={{bottom: 50, left: 330}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  ) : (
                    <Ionicons
                      name="eye-outline"
                      color="#ADADAF"
                      size={15}
                      style={{bottom: 50, left: 330}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  )}
                  <Button
                    onPress={() => forgotPasswordefBottomModal.current.open()}
                    content={
                      <Text style={{alignSelf: 'flex-end', color: colors.text}}>
                        Forgot Password ?
                      </Text>
                    }
                  />
                  {authError.error ? (
                    <Text>{authError.errorMessage}</Text>
                  ) : null}
                  <Button
                    onPress={() => {
                      email.length && password.length > 0
                        ? handleLogin(email, password)
                        : Alert.alert(
                            'Introduce un Correo y Contraseña Validos',
                          );
                    }}
                    content={
                      auth ? (
                        <Ionicons
                          name="checkmark-outline"
                          color="white"
                          size={30}
                        />
                      ) : authLoading ? (
                        <ActivityIndicator size="small" />
                      ) : (
                        <Text>Log in</Text>
                      )
                    }
                    styleBtn={{
                      backgroundColor: 'lightblue',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingVertical: 20,
                      borderRadius: 10,
                      width: '95%',
                      marginTop: 30,
                    }}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      // backgroundColor: 'yellow',
                      marginTop: 40,
                    }}>
                    <Text style={{color: colors.text, marginRight: 4}}>
                      Don't have an account?
                    </Text>
                    <Button
                      onPress={() => {
                        setAuthError(prevState => ({
                          ...prevState,
                          error: false,
                          errorMessage: '',
                        }));
                        signinrefBottomModal.current.open();
                      }}
                      content={<Text style={{color: 'purple'}}>Sign In</Text>}
                    />
                  </View>
                </View>
              </View>
            </View>
            {signinModal()}
            {forgotPasswordModal()}
          </View>
        }
      />
    );
  };

  const forgotPasswordModal = () => {
    return (
      <BottomModal
        openModal={forgotPasswordefBottomModal}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={400}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        content={
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                padding: 15,
              }}>
              <AntDesign
                onPress={() => {
                  setAuthError(prevState => ({
                    ...prevState,
                    error: false,
                    errorMessage: '',
                  }));
                  forgotPasswordefBottomModal.current.close();
                }}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                height: '80%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <Text
                  style={{
                    fontSize: 15,
                    marginLeft: 4,
                    color: colors.text,
                    textAlign: 'center',
                    marginBottom: 35,
                  }}>
                  Your email to reset password
                </Text>
                <View>
                  <TextInput
                    value={email}
                    onChangeText={value => setEmail(value)}
                    placeholder="Email"
                    placeholderTextColor="#ADADAF"
                    style={{
                      backgroundColor: colors.forms,
                      color: colors.text,
                      paddingVertical: 15,
                      paddingHorizontal: 25,
                      width: 370,
                      height: 65,
                      borderRadius: 10,
                      marginVertical: 10,
                    }}
                  />

                  <Button
                    onPress={() => {
                      email.length > 0
                        ? handleSendEmailToResetPassword(email)
                        : Alert.alert(
                            'Introduce un Correo para enviar el mail',
                          );
                    }}
                    content={
                      auth ? (
                        <Ionicons
                          name="checkmark-outline"
                          color="white"
                          size={30}
                        />
                      ) : authLoading ? (
                        <ActivityIndicator size="small" />
                      ) : (
                        <Text>Send</Text>
                      )
                    }
                    styleBtn={{
                      backgroundColor: 'gray',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingVertical: 20,
                      borderRadius: 10,
                      width: '95%',
                      marginTop: 30,
                    }}
                  />
                </View>
              </View>
            </View>
          </View>
        }
      />
    );
  };

  const signinModal = () => {
    return (
      <BottomModal
        openModal={signinrefBottomModal}
        wrapperColor={colors.subModalWrapper}
        muchContent={true}
        customSize={true}
        sizeModal={840}
        borderRadiusTop={10}
        keyBoardPushContent={false}
        closeDragDown={false}
        content={
          <View style={{flexDirection: 'column'}}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-end',
                // backgroundColor: 'red',
                padding: 15,
              }}>
              <AntDesign
                onPress={() => {
                  setAuthError(prevState => ({
                    ...prevState,
                    error: false,
                    errorMessage: '',
                  }));
                  signinrefBottomModal.current.close();
                }}
                name="close"
                color={colors.text}
                size={30}
              />
            </View>
            <View
              style={{
                // backgroundColor: 'blue',
                height: '87%',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <View
                  style={{
                    marginBottom: 50,
                    // backgroundColor: 'pink',
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Ionicons name="school" color={colors.text} size={45} />
                  <Text
                    style={{fontSize: 38, marginLeft: 4, fontWeight: 'bold'}}>
                    Skool
                  </Text>
                </View>

                <View>
                  <Input
                    formInput={true}
                    inputValue={name}
                    inputValueOnChange={value => setName(value)}
                    placeHolder="Name"
                  />
                  <Input
                    formInput={true}
                    inputValue={email}
                    inputValueOnChange={value => setEmail(value)}
                    placeHolder="Email"
                  />
                  <Input
                    formInput={true}
                    inputValue={userName}
                    inputValueOnChange={value => setUserName(value)}
                    placeHolder="Username"
                  />
                  <Input
                    formInput={true}
                    inputValue={password}
                    inputValueOnChange={value => setPassword(value)}
                    placeHolder="Password"
                    customSecureTextEntry={viewPassword ? false : true}
                  />

                  {viewPassword ? (
                    <Ionicons
                      name="eye-off-outline"
                      color="gray"
                      size={15}
                      style={{bottom: 50, left: 325}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  ) : (
                    <Ionicons
                      name="eye-outline"
                      color="gray"
                      size={15}
                      style={{bottom: 50, left: 325}}
                      onPress={() => setViewPassword(!viewPassword)}
                    />
                  )}

                  {authError.error ? (
                    <Text>{authError.errorMessage}</Text>
                  ) : null}
                  <Button
                    onPress={() => {
                      name.length &&
                      userName.length &&
                      email.length &&
                      password.length > 0
                        ? handleRegister(email, password, userName, name)
                        : Alert.alert('Introduce Correo, Contraseña');
                    }}
                    content={
                      auth ? (
                        <AntDesign name="checkcircle" color="white" size={30} />
                      ) : authLoading ? (
                        <ActivityIndicator size="small" />
                      ) : (
                        <Text>Sign in</Text>
                      )
                    }
                    styleBtn={{
                      backgroundColor: 'lightblue',
                      alignItems: 'center',
                      alignSelf: 'center',
                      paddingVertical: 20,
                      borderRadius: 10,
                      width: '95%',
                      marginTop: 30,
                    }}
                  />

                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      // backgroundColor: 'yellow',
                      marginTop: 40,
                    }}>
                    <Text style={{marginRight: 4}}>Have an account?</Text>
                    <Button
                      onPress={() => signinrefBottomModal.current.close()}
                      content={<Text style={{color: 'purple'}}>Login</Text>}
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        }
      />
    );
  };

  return (
    <View>
      {customButton ? (
        <Button
          onPress={
            isLoggedIn(realmApp)
              ? onPress
              : () => loginrefBottomModal.current.open()
          }
          content={customButton}
        />
      ) : (
        <Button
          onPress={
            isLoggedIn(realmApp)
              ? onPress
              : () => loginrefBottomModal.current.open()
          }
          content={
            <AntDesign name="pluscircle" size={iconSize} color="#59EEFF" />
          }
        />
      )}
      {loginModal()}
      {/* {customButton ? (
        <Button onPress={onPress} content={customButton} />
      ) : (
        <Button
          onPress={onPress}
          content={
            <AntDesign name="pluscircle" size={iconSize} color="#59EEFF" />
          }
        />
      )} */}
    </View>
  );
};

export default AddButton;
